<h1 align="center">Awesome Tools 😎</h1>

This repo has the purpose of listing privacy-friendly tools, software, websites and operating systems that I recommend/use.

## Table of contents

- [Before proceeding](#before-proceeding)
- [Operating Systems to choose](#operating-systems-to-choose)
- [Software](#software)
  - [Media](#media)
  - [Emulation](#emulation)
  - [Office and Documents](#office-and-documents)
  - [Programming](#programming)
    - [IDEs](#ides)
  - [Web Browsers](#web-browsers)
    - [Extensions](#extensions)
  - [Misc](#misc)
  - [OS-strict software](#os-strict-software)
    - [Android](#android)
    - [Windows 10](#windows-10)
- [Websites and services](#websites-and-services)
- [License](#license)

## Before proceeding

- This list is based on software that I use daily and that suits my workflow.
- There are no guaratee that the software/servies in this list have/give absolute privacy. This list has tools that aim to minimize data collection and telemetry.
- If you want more privacy, I recommend switching to Free and Open Source Software alternatives to proprietary software.
  - This list only contains Free and Open Source Software.

**_And, a great thanks to all those invoved in the Free Software community for these pieces of awesome work!_**

## Operating Systems to choose

If you want to have more privacy on your everyday computing life, I recommend switching to an operating system that uses a `GNU + Linux kernel`.

I personally use [Void Linux](https://voidlinux.org) as my OS. Void has an awesome package manager called `xbps` and uses `runit` as the init system.

You can choose from a variety of Linux distros out there, the one that suits your use.

A great begginer-friendly distro is [Linux Mint](https://linuxmint.com/) (it was mine :p).

**Note:** these OSs have non free/proprietary code. If you want to use an OS that has absolute no proprietary software, the GNU Project site [lists](http://www.gnu.org/distros/free-distros.html) 100% free distros. [More on GNU and free software philosophy.](http://www.gnu.org/philosophy/philosophy.html)

## Software

### Media

- [**FreeTube**](https://github.com/FreeTubeApp/FreeTube) - A desktop frontend for YouTube.
- [**youtube-dl**](https://github.com/ytdl-org/youtube-dl) - CLI YouTube and many more video-hosting platforms content downloader.
- [**Rhythmbox**](https://gitlab.gnome.org/GNOME/rhythmbox) - GNOME Music player.
- [**mpv**](https://github.com/mpv-player/mpv) - Video player.
- [**pulsemixer**](https://github.com/GeorgeFilipkin/pulsemixer) - Simple CLI Mixer.

### Emulation

- [**RetroArch**](https://github.com/libretro/RetroArch) - Frontend for a LOT of emulators.
- [**Snes9x**](https://github.com/snes9xgit/snes9x) - SNES emulator.
- [**BSNES**](https://github.com/bsnes-emu/bsnes) - High-accuracy SNES emulator.
- [**mGBA**](https://github.com/mgba-emu/mgba) - GBA, GB and GBC emulator.
- [**Flips**](https://github.com/Alcaro/Flips) - Floating IPS. Used to patch .bps files into SNES roms.

### Office and Documents

- [**LibreOffice**](https://www.libreoffice.org/) - A full-featured FOSS office suite.
- [**PDFsam**](https://github.com/torakiki/pdfsam) - Split, merge, rotate, mix, etc. PDF files.
- [**TeXStudio**](https://www.texstudio.org/) - Full-featured LaTeX editor.

### Programming

#### IDEs

- [**VSCodium**](https://github.com/VSCodium/vscodium) - Visual Studio Code fork, sans Microsoft's telemetry.
- [**vim**](https://github.com/vim/vim) - Advanced text editor.
- [**Eclipse IDE**](https://www.eclipse.org/) - Java development.

### Web Browsers

- **Firefox** - Use with a hardened profile for privacy (otherwise [spyware](https://spyware.neocities.org/articles/firefox.html)).
  - [**ffprofile**](https://ffprofile.com) - Firefox profile maker.
- [**Ungoogled Chromium**](https://github.com/Eloston/ungoogled-chromium) - Chromium sans Google's integration.
- [**TOR Browser**](https://www.torproject.org/)

#### Extensions

For all browsers above.

- [**uMatrix**](https://github.com/gorhill/uMatrix) - Block/allow each web request. `Repo archived, but this is THE extension for maximum web requests control.`
- [**uBlock Origin**](https://github.com/gorhill/uBlock) - Ad and page elements blocker.
- [**Decentraleyes**](https://git.synz.io/Synzvato/decentraleyes) - Local emulation of CDNs.

### Misc

- [**ksnip**](https://github.com/ksnip/ksnip) - Screenshot tool.
- [**KeePassXC**](https://github.com/keepassxreboot/keepassxc) - Password manager.

### OS-strict software

#### Android

- [**F-Droid**](https://f-droid.org/) - An app store for Open Source Software.

`All software listed here can be found on F-Droid.`

- [**Tracker Control**](https://github.com/OxfordHCC/tracker-control-android) - A "firewall" app. It allows you to block trackers, internet access and within an app site requests.
- [**FairEmail**](https://github.com/M66B/FairEmail) - Email client.
- [**Florisboard**](https://github.com/florisboard/florisboard) - Privacy respecting keyboard.
- [**Binary Eye**](https://github.com/markusfisch/BinaryEye) - QR-code scanner.
- [**NewPipe**](https://github.com/TeamNewPipe/NewPipe) - Privacy-oriented YouTube frontend.
- [**Infinity**](https://github.com/Docile-Alligator/Infinity-For-Reddit) - Reddit client.
- [**Lemmur**](https://github.com/krawieck/lemmur) - An app for browsing [Lemmy](https://github.com/LemmyNet/lemmy) commiunities.
- [**Material Files**](https://github.com/zhanghai/MaterialFiles) - File manager.
- [**Librera PRO**](https://github.com/foobnix/LibreraReader) - Full-featured PDF and E-PUB reader.
- [**Private Location**](https://github.com/wesaphzt/privatelocation) - Mask your location.
- [**Geometric Weather**](https://github.com/WangDaYeeeeee/GeometricWeather) - Weather app.
- [**Scrambled Exif**](https://gitlab.com/juanitobananas/scrambled-exif) - Remove exif metadata from images.
- [**Red Moon**](https://github.com/LibreShift/red-moon) - Blue-light filter.
- [**KeePassDX**](https://github.com/Kunzisoft/KeePassDX) - KeePass passord manager.
- [**Greentooth**](https://gitlab.com/nbergman/greentooth) - Disables bluetooth automatically.
- [**Fokus**](https://github.com/icabetong/fokus) - Student-oriented tasks app.
- [**Android OCR**](https://github.com/SubhamTyagi/android-ocr) - OCR app for Android.

#### Windows 10

- [**Simplewall**](https://github.com/henrypp/simplewall) - Firewall software. It can detect any Windows 10's request to the internet. `Repo archived.`
- [**SumatraPDF**](https://github.com/sumatrapdfreader/sumatrapdf) - Small and light PDF viewer.

## Websites and services

- [**Lemmy**](https://github.com/LemmyNet/lemmy) - A federated Reddit-like service.
- [**Invidious**](https://github.com/iv-org/invidious) - YouTube frontend.
- [**Neocities**](https://neocities.org) - Geocities-like personal website hosting service.
- [**Fosscord**](https://github.com/fosscord/fosscord) - FOSS Discord-like chat, voice and video paltform. `Still in development`

## License

![](./img/gpl3.png)

```
Copyright (C) 2021 Shobon03

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
```

LAST UPDATED ON: 2021/07/25
